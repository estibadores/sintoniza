---
title: streaming directo
---

# Streaming video

Servido a traves de sintoniza.sindominio.net un canal para hacer streming video y audio.

Usaremos el estandar [RTMP](https://en.wikipedia.org/wiki/Real-Time_Messaging_Protocol) Real-Time Messaging Protocol.

En nuestro caso, además del servidor de Streaming RTMP+HLS hemos incluido un pequeño portal de visualización del video emitido a través de un webplayer basado en VideoJS.

# Uso

Puedes emitir usando OBS, como se comenta en un apartado inferior. 

Para hacer las pruebas rápidamente, puedes usar 'ffmpeg'.

Para ello, emitiremos un video que tengamos en nuestro computador, por ejemplo, 'video.mp4'.

Usaremos como punto de montaje/canal el nombre 'prueba'. Si lo usáis más de una persona, solo le funcionará a una.

~~~
$ ffmpeg -re -stream_loop -1 -i 'video.mp4' -vcodec libx264 -vprofile baseline -g 30 -acodec aac -strict -2 -f flv 'rtmp://sintoniza.sindominio.net/entrada/prueba'
~~~

Ahora, para poderlo visualizar, podrías acceder a la web de Sintoniza.

https://sintoniza.sindominio.net#prueba

O, puedes abrirlo con VLC o mpv usando el fichero _m3u8_ que genera la emisión:

~~~
$ mpv https://sintoniza.sindominio.net/canal/prueba.m3u8
~~~

# Creación del Dockerfile

## Install rtmp mod for nginx

```bash
apt install libnginx-mod-rtmp
```

## Preparar nginx para estrimear en `rtmp`

_Nginx puede estrimear codecs de HSL y Dash/MPEG, en **este ejemplo** únicamente veremos **HLS**_

en `/etc/nginx/nginx.conf` añadimos la configuración del fichero `nginx-rtmp.conf`


## Prepara los directorios para el streaming

La pagina se sirve desde `/var/www/html/` para automatizar el proceso de creación de Docker y usar la configuración por defecto de Nginx en Debian.

Crea el subdirectorio `/var/www/html/hls` donde los trozos de vídeos serán almacenados:

```bash
mkdir -P /var/www/html/live.sindominio.net/hls
```
le damos permisos de lectura, escritura y ejecución a cualquier user ya que montaremos el volumen fuera del docker
```bash
chmod 777 /var/www/html/hls
```


## Prepara tu web

Ponemos la página `index.html` en `/var/www/html/` para que la sirva por defecto.

Esta página llama a los ficheros js que permiten recibir y enviar video así como los css que dan forma gráfica y de usabilidad a la página.

Por organización y limpieza creamos el directorio `assets/` donde pondremos los siguiente ficheros:

* styles-sintoniza.css  - diseño de nuestra página
* video-js.min.css - estilos del player (botones, ventana, volumen, etc...)
* video-js.min.css  - interacción js del player, tanto para recibir como para enviar video
* jquery-slim.min.js  - dependencia de jquery por parte del js del player

obtención de los ficheros:
**styles-sintoniza.css** los escribimos.

**videojs**: iremos a https://github.com/videojs/video.js/releases, bajaremos la última versión pero utilizaremos únicamente `video-js.min.css` y `video.min.js`. Si queremos traducciones también el directorio `lang/`

**jquery** : Bajaremos la última versión minimizada desde https://jquery.com/download/ y la renombraremos `jquery-slim.min.js`


## Servir la nueva web con nginx

_en este ejemplo asumimos que ya tienes los certificados creados con letsencrypt_

En el fichero por defecto añadimos el siguiente codigo
`/etc/nginx/sites-available/default` añade bajo el bloque `443 ssl`:

```
    location /hls {
      # Disable cache
      add_header Cache-Control no-cache;

      # CORS setup
      add_header 'Access-Control-Allow-Origin' '*' always;
      add_header 'Access-Control-Expose-Headers' 'Content-Length';

      # allow CORS preflight requests
      if ($request_method = 'OPTIONS') {
        add_header 'Access-Control-Allow-Origin' '*';
        add_header 'Access-Control-Max-Age' 1728000;
        add_header 'Content-Type' 'text/plain charset=UTF-8';
        add_header 'Content-Length' 0;
        return 204;
      }

      types {
        application/vnd.apple.mpegurl m3u8;
        video/mp2t ts;
      }
    }
```

## Últimos pasos

### En el servidor
* controla tu firewall y permite la entrada de trafico por el puerto 1935
* haz que `/var/www/html/live.sindominio.net/` y sus contenidos puedan ser leidos por `www-data`
* comprueba y reinicia nginx `nginx -t` y luego `nginx -s reload`


### En tu ordenador

ahora puedes instalarte [OBS](https://obsproject.com/)

`apt install obs-studio`

Envia el directo a `rtmp://live.sindominio.net:1935/entrada/stream_key` y miralo en `https://live.sindominio.net/#stream_key`

# Extra

##  `/etc/nginx/nginx.conf`

### configuraciones en el bloque `rtmp`

* Nginx te permite ajustar el HLS, instrucciones: https://nginx.org/en/docs/http/ngx_http_hls_module.html
* Si quieres que la gente pueda recibir el stream directamente comenta la línea "deny play all;"

* Puedes enviar el stream recibido directammente en `rtmp://live.sindominio.net/entrada/stream_name` a un servicio externo como _youtube.com_ añadiendo `push rtmp://a.rtmp.youtube.com/live2/stream_key;`
* Puedes enviar y recibir streams simultáneamente a otros lugares o páginas web añadiendo más bloques de `aplicacones` con diferentes nombres

## Otras referencias

- https://c3voc.de/wiki/distributed-conference

## traducción de los mensajes de _videojs_

Al pie de `index.html` o dónde tengas la llamada a

```html
<script>
  $(function() {
   if (window.location.hash) {
...

   } else {
...

   }
  });
</script>
```

añade este código _js_ dentro de `if (window.location.hash){ }`

```html
<script>
  $(function() {
   if (window.location.hash) {

//some code here ...

        videojs.addLanguage("es",{
          "You aborted the media playback": "Has interrumpido la reproducción del vídeo.",
          "A network error caused the media download to fail part-way.": "Un error de red ha interrumpido la bajada del vídeo.",
          "The media could not be loaded, either because the server or network failed or because the format is not supported.": "La emisión no está activa en este momento.",
          "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.": "La reproducción del video se ha interrumpido debido a que el archivo estaba corrompido o por que tu navegador no soporta este formato.",
          "No compatible source was found for this media.": "No se ha encontrado ningún origen para este medio."
        });
        videojs.addLanguage("ca",{
          "You aborted the media playback": "Heu interromput la reproducció del vídeo.",
          "A network error caused the media download to fail part-way.": "Un error de la xarxa ha interromput la baixada del vídeo.",
          "The media could not be loaded, either because the server or network failed or because the format is not supported.": "La emissió està inactiva.",
          "The media playback was aborted due to a corruption problem or because the media used features your browser did not support.": "La reproducció de vídeo s'ha interrumput per un problema de corrupció de dades o bé perquè el vídeo demanava funcions que el vostre navegador no ofereix.",
          "No compatible source was found for this media.": "No s'ha trobat cap font compatible amb el vídeo."
        });

//some code here ...

   } else {

//some code here ...

   }
  });
</script>
```

# Crear el stream de directos

Seguimos los pasos documentados en la trastienda
https://sindominio.net/trastienda/tecnica/servicios/streamingdirecto/

Incluir index.html,video-js.css, video.js and jquery-slim.min.js y ya lo tienes.

# Utilidades

Se adjunta un script _clean.sh_ para limpiar periódicamente la carpeta de las emisiones, por alguna razón el modulo de nginx no parece que en algunos casos esté haciendo un _cleanup_ de los fragmentos de HLS antiguos y el disco se llena.

Para ejecutarlo, se puede configurar en el Cron del host que contiene el demonio de Docker:

~~~
# crontab -e
@hourly	docker exec <nombre_container> /etc/cron.hourly/clean.sh
~~~

# Como hacer que jitsi haga stream en sintoniza

## solucion 1: hackeando jitsi
estable y funciona desde hace un tiempo según su autor:
https://github.com/jitsi/jitsi-meet/issues/2829#issuecomment-509712706

## solución 2: con un script ffmpeg
solucion no intusiva con jitis y permite striming de cualquier fuents.

https://community.jitsi.org/t/how-do-i-change-youtube-live-stream-to-another-rtmp-server-url/24817/7?u=lodopidolo

## solución 3: simulcast a cualquier otra págia

permite enviar simultáneamente a muchos streamers
https://community.jitsi.org/t/stream-to-any-or-multiple-rtmp-destinations-record-simultaneously/51943

## solución 4: rtmp nginix video por exo/guifipedro
https://gitlab.com/guifi-exo/wiki/-/blob/master/howto/rtmp_streamer.md

# Linkografia

Hemos seguido los pasos documentados por [guifi.net + exo](https://gitlab.com/guifi-exo) y explicados aqui (https://gitlab.com/guifi-exo/wiki/-/blob/master/howto/real-time-media/rtmp_streamer.md)


