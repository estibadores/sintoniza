FROM registry.sindominio.net/nginx

RUN apt-get update -y && \
	apt-get install -y libnginx-mod-rtmp cron \
	ffmpeg wget

RUN rm -rf /var/www/html/*

# config nginx with rtmp app points
WORKDIR /tmp/
COPY nginx-rtmp.conf /tmp/
RUN cat /tmp/nginx-rtmp.conf >> /etc/nginx/nginx.conf
RUN rm -rf /tmp/*

ADD clean.sh /etc/cron.hourly/

RUN mkdir -p /var/www/html/hls && \
    chmod 777 /var/www/html/hls && \
    mkdir -p /var/www/html/canal && \
    chmod 777 /var/www/html/canal

# poner el index.html css y js
COPY index.html /var/www/html/

RUN mkdir /var/www/html/rtmp/ && \
	wget https://raw.githubusercontent.com/arut/nginx-rtmp-module/master/stat.xsl -O /var/www/html/rtmp/stat.xsl

RUN mkdir -p /var/www/html/assets/
COPY assets/styles-sintoniza.css /var/www/html/assets/
COPY assets/video-js.min.css /var/www/html/assets/video-js.min.css 
COPY assets/video.min.js /var/www/html/assets/video-js.min.js
COPY assets/jquery-3.6.1.slim.min.js /var/www/html/assets/jquery-slim.min.js

# copiar la configuración por defecto de nginx con nuestros cambios incluidos
COPY nginx-site-default.conf /etc/nginx/sites-enabled/default 

